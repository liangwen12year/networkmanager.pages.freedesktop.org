---
title: 'NetworkManager'
---

The NetworkManager daemon attempts to make networking configuration as painless and automatic as possible while offering an extensive and rich API to interact with Linux networking.
